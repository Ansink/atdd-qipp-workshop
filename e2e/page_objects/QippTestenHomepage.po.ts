//Jeroen Ansink, QIPP Testen BV 2018

import {$, $$, by, element, ElementFinder, ElementArrayFinder, browser} from 'protractor';

export class Homepage {
    qippLogo: ElementFinder;

    constructor() {
        this.qippLogo = $('#comp-jgezvtycimgimage');
    }

    socialLink(socialLinkType:string): ElementFinder {
      switch (socialLinkType) {
        case 'Facebook':
          return element(by.css('#comp-jggexkig0imageimageimage'));
        case 'Twitter':
          return element(by.css('#comp-jggexkig1imageimageimage'));
        case 'LinkedIn':
          return element(by.css('#comp-jggexkig2imageimageimage'));
      }
    }

    formSubmitButton(): ElementFinder {
      return element(by.xpath('//button/span[contains(text(), "Nu gratis abonneren")]/..'));
    }

    formSuccessMessage(): ElementFinder {
      return element(by.xpath('//span/h3[contains(text(), "Bedankt voor het abonneren!")]'));
    }

    formErrorMessage(): ElementFinder {
      return element(by.xpath('//span[contains(text(), "Vul een geldig e-mailadres in")]'));
    }

    formErrorMessage2(): ElementFinder {
      return element(by.xpath('//span[contains(text(), "Vul alle verplichte velden in")]'));
    }

    formNameField(): ElementFinder {
      return element(by.xpath('/html/body/div/get-subscribers/div/div/div[2]/div[1]/input'));
    }

    formMailField(): ElementFinder {
      return element(by.xpath('/html/body/div/get-subscribers/div/div/div[2]/div[2]/input'));
    }

    employeeName(employee: string): ElementFinder {
      switch (employee) {
        case 'Frank Engelkes':
          return element(by.xpath('//*[@id="comp-jgg5hgen"]/h2/span/span/span'));
        case 'Johan de Bruijn':
          return element(by.xpath('//*[@id="comp-jgg5hgfq"]/h2/span/span/span'));
        case 'Helene van Bommel':
          return element(by.xpath('//*[@id="comp-jgg5hggg"]/h2/span/span/span'));
        case 'Jeroen Ansink':
          return element(by.xpath('//*[@id="comp-jgg5i5yh"]/h2/span/span/span'));
        case 'Joost de Jong':
          return element(by.xpath('//*[@id="comp-jgg5i77r"]/h2/span/span/span'));
      }
    }
}
