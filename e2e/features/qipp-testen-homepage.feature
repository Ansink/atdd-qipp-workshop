#Jeroen Ansink, QIPP Testen BV 2018

Feature: The website of Qipp Testen
  In order to reach customers
  As director of Qipp Testen
  I want a clean and nice website

Scenario: The Qipp Testen logo is present on the website
  Given The Qipp Testen website is loaded
  Then the Qipp Testen logo is visible

Scenario Outline: The Qipp Testen employee <employee> is present on the website
  Given The Qipp Testen website is loaded
  Then the Qipp Testen employee <employee> is present on the website
  Examples:
  | employee          |
  | Frank Engelkes    |
  | Johan de Bruijn   |
  | Helene van Bommel |
  | Jeroen Ansink     |
  | Joost de Jong     |

Scenario: There are social links present on the website
  Given The Qipp Testen website is loaded
  Then the social link <SOCIAL> is present on the website
  | SOCIAL   |
  | Facebook |
  | Twitter  |
  | LinkedIn |

Scenario Outline: The user fills in the form for the newsletter
  Given The Qipp Testen website is loaded
  When The user fills in the name: <name>
  And The user fills in the e-mail adress: <mail>
  And The user clicks on "Nu gratis abonneren"
  Then The message <message> is shown to the user
  Examples:
  | name          | mail                 | message                       |
  | Jeroen Ansink | jeroen.ansink@me.com | Bedankt voor het abonneren!   |
  | Joost de Jong | joost@qipp-testen    | Vul een geldig e-mailadres in |
  |               |                      | Vul alle verplichte velden in |
