//Jeroen Ansink, QIPP Testen BV 2018

import {Given, When, Then, setDefaultTimeout} from 'cucumber';
import {element, by, browser, ExpectedConditions, protractor} from 'protractor';
import {Homepage} from '../page_objects/QippTestenHomepage.po';
const chai = require('chai');
const chaiString = require('chai-string');
const chaiSmoothie = require('chai-smoothie');

chai.use(chaiSmoothie)
    .use(chaiString);
const expect = chai.expect;
const homepage = new Homepage();

setDefaultTimeout(60 * 1000);

Given(/^The Qipp Testen website is loaded$/, async () => {
  await browser.get('');
});

Then(/^the Qipp Testen logo is visible$/, async () => {
  await browser.switchTo().defaultContent();
  await browser.sleep(2000);
  await expect(homepage.qippLogo).to.be.displayed;
});

When(/^The user fills in the name: ([^"]*)$/, async(name:string) => {
  await browser.switchTo().frame(element(by.id('comp-jh0yn8xmiframe')).getWebElement());
  await browser.sleep(3000);
  await homepage.formNameField().sendKeys(name);
  await browser.switchTo().defaultContent();
  await browser.sleep(3000);
});

When(/^The user fills in the e-mail adress: ([^"]*)$/, async(mail:string) => {
  await browser.switchTo().frame(element(by.id('comp-jh0yn8xmiframe')).getWebElement());
  await browser.sleep(3000);
  await homepage.formMailField().sendKeys(mail);
  await browser.switchTo().defaultContent();
  await browser.sleep(3000);
});

Then(/^the Qipp Testen employee ([^"]*) is present on the website$/, async(employee:string) => {
  await expect(homepage.employeeName(employee)).to.be.displayed;
});

When(/^The user clicks on "Nu gratis abonneren"$/, async() => {
  await browser.switchTo().frame(element(by.id('comp-jh0yn8xmiframe')).getWebElement());
  await browser.sleep(3000);
  await homepage.formSubmitButton().click();
  await browser.switchTo().defaultContent();
  await browser.sleep(3000);
});

Then(/^The message ([^"]*) is shown to the user$/, async(message:string) => {
  await browser.switchTo().frame(element(by.id('comp-jh0yn8xmiframe')).getWebElement());
  await browser.sleep(3000);

  switch (message) {
    case 'Bedankt voor het abonneren!':
      await expect(homepage.formSuccessMessage()).to.be.displayed
      await browser.switchTo().defaultContent();
      await browser.sleep(3000);
      break;
    case 'Vul alle verplichte velden in':
      await expect(homepage.formErrorMessage2()).to.be.displayed
      await browser.switchTo().defaultContent();
      await browser.sleep(3000);
      break;
    default:
      await expect(homepage.formErrorMessage()).to.be.displayed
      await browser.switchTo().defaultContent();
      await browser.sleep(3000);
      break;
  }
});

Then(/^the social link ([^"]*) is present on the website$/, async (link, dataTable) => {
    const errors: string[] = [];
    for (const sociallink of dataTable.hashes()) {
        try {
            await expect(await homepage.socialLink(sociallink.SOCIAL)).to.be.displayed;
        } catch (e) {
            errors.push(e.stack.split('\n')[0]);
            console.log(e.stack.split('\n')[0]);
        }
    }
    expect(errors, JSON.stringify(errors, null, 3)).to.be.empty;
});
